package com.codistan.java.Suleyman.lesson07;

public class Arrays {

	public static void main(String[] args) {
	
		int myIntegers[]= new int[4];
		
		myIntegers[0]=10;
		myIntegers[1]=20;
		
		
		System.out.println(myIntegers[0]);
		System.out.println(myIntegers[1]);
		System.out.println(myIntegers[3]);

		
		int[] myIntegersTwo = {10, 20, 30, 40};
		
		System.out.println(myIntegersTwo[0]);
		System.out.println(myIntegersTwo[1]);
		System.out.println(myIntegersTwo[3]);
		
		/*Create two arrays; one for grocery item names, one for grocery item prices as following: lemon, lettuce, watermelon, orange, spinach and 5.99, 2.99, 1.89, 8.00, 2.89 and print each item�s name and price on the same line.
		 */
		String itemNames[] = {"Lemon" , "Lettuce" , "Watermelon" , "Orange" , "Spinach" };
		
		double[] itemPrices = {5.99, 2.99 , 1.89, 8.00, 2.89};
		
		System.out.println(itemNames[0] + " " + itemPrices[4] + "$");
		System.out.println(itemNames[1] + " " + itemPrices[3] + "$");
		System.out.println(itemNames[2] + " " + itemPrices[2] + "$");
		System.out.println(itemNames[3] + " " + itemPrices[1] + "$");
		System.out.println(itemNames[4] + " " + itemPrices[0] + "$");
		
		 
		System.out.println(itemPrices.length-1);
		
		System.out.println(itemPrices.toString());
		
		int [] nums = {3, 5, 6};
		
		 int len = nums.length*2;
		 
		 int[] dubsArr = new int[len];
		 
		 dubsArr[len-1]=nums[nums.length-1];
		 
		 System.out.println(dubsArr.toString());
		
              System.out.println(dubsArr);
		
		
		

		
	}

	
}