 package com.codistan.java.Suleyman.lesson07;

import java.util.ArrayList;

public class ArraysListExample {

	public static void main(String[] args) {
		
		ArrayList<String> myArrayList = new ArrayList<String>();
		
		myArrayList.add("Hello");
		myArrayList.add("World");
		myArrayList.add("!");
		System.out.println(myArrayList);
		
		myArrayList.remove("!");
        System.out.println(myArrayList); 
        
        myArrayList.remove(0);
        System.out.println();
        
        System.out.println(myArrayList.size());
        
        System.out.println(myArrayList.get(0));
        
        System.out.println(myArrayList.contains("World"));
        System.out.println(myArrayList.contains("Hello"));
        
        System.out.println(myArrayList.indexOf("World"));
        
	}

}
