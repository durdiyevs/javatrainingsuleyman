package com.codistan.java.Suleyman.lesson07;

import java.util.Scanner;

public class FlowControl {

	public static void main(String[] args) {
		
		/*
		 10% VAT and $20 discount for females
		 */
		Scanner keyboard = new Scanner(System.in);
		int price = 100;
		double total = 0;
		double tax = 0.1;
		boolean storeOpen = true;
		boolean member = true;
		if (storeOpen) {
			System.out.println("Enter your gender M / F");
			String gender = keyboard.nextLine();
			System.out.println("Enter your age");
			int age = keyboard.nextInt();
			System.out.println("are you a member Y / N");
			String membership = keyboard.nextLine();
			if(membership.equals("Y")) {
				member = true;
			}else {
				member = false;
			}
			if (gender.equals("M")) {
				if (member) {
					total = price + (price * tax) - 5;
				} else {
					total = price + (price * tax);
				}
			} else {
				if (age < 18) {
					total = price + (price * tax) - 20 - 10;
				} else {
					total = price + (price * tax) - 20;
				}
			}
		} else {
			System.out.println("store is closed");
		}
		System.out.println("total amount due is: "+total);
	}

}
  
	
	/* boolean isHungry = true; 
		boolean allowedToEat = true;
	 
	if(isHungry) {
	allowedToEat = true; // if statement
}else {
	allowedToEat = false; // else statement
}
System.out.println(allowedToEat);

if(!isHungry) {
	allowedToEat = true; // if statement
}else {
	allowedToEat = false; // else statement
}
System.out.println(allowedToEat);

} */