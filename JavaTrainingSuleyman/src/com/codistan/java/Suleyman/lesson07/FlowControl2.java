package com.codistan.java.Suleyman.lesson07;

import java.util.Scanner;

public class FlowControl2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Scanner keyboard = new Scanner(System.in);
		System.out.println("Please enter your weight in kg");
		double weight = keyboard.nextDouble();
		System.out.println("Please enter your height in meter");
		double height = keyboard.nextDouble();
		double bmi = weight / (height * height);
		System.out.println("Your BMI is " + bmi);
	   
		if ( bmi < 18.5) {
			System.out.println("You are  UnderWeight ");
		}
		else if ( bmi >= 18.5 && bmi <= 24.9  ) {
			System.out.println("You are  NormalWeight ");
	   }
		else if ( bmi > 25 && bmi < 29.9 ) {
			System.out.println("You are OverWeight ");
      }
		else if ( bmi >= 30 ) {
			System.out.println("Obesity ");
      }
	}
}

/*System.out.println("Enter your Grade");
int gradeOfStudent = keyboard.nextInt();

if (gradeOfStudent > 90) {
	System.out.println("A");
}
else if (gradeOfStudent > 80) {
	System.out.println("B");
}
else if (gradeOfStudent > 70) {
	System.out.println("C");
}
else if (gradeOfStudent > 60) {
	System.out.println("D");
}
else if (gradeOfStudent > 50) {
	System.out.println("E");
}
else  {
	System.out.println("F");
}*/