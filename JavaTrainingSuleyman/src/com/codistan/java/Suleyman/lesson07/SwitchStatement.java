package com.codistan.java.Suleyman.lesson07;

public class SwitchStatement {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		String grade = "C";
		switch(grade) {
		case "A":
			System.out.println("Perfect");
			break;
		case "B":
			System.out.println("Very good");
			break;
		case "C":  // if a case is not defined with a statement next case's statement will be executed
		case "D":
			System.out.println("You did Ok");
			break;
		default:
			System.out.println("Study harder");
			break;
		}

	}

}
