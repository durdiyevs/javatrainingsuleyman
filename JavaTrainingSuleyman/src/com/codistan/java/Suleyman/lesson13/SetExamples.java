package com.codistan.java.Suleyman.lesson13;

import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.NavigableSet;
import java.util.TreeSet;

public class SetExamples {

	public static void main(String[] args) {
		
		HashSet<String> myHashSet = new HashSet<>(); 
		myHashSet.add("Orange");
		myHashSet.add("Banana");
		myHashSet.add("Apple");
		
		System.out.println(myHashSet);
		
		LinkedHashSet<String> myLinkedHashSet = new LinkedHashSet<>(); 
		myLinkedHashSet.add("Orange");
		myLinkedHashSet.add("Banana");
		myLinkedHashSet.add("Apple");
		
		System.out.println(myLinkedHashSet);
		
		LinkedHashSet<String> lhs2 = new LinkedHashSet<>(myHashSet);
		System.out.println(lhs2);
		lhs2.add("Watermelon");
		lhs2.add("Fig");
		lhs2.add("Grape");
		System.out.println(lhs2);
		
		TreeSet<String> ts = new TreeSet<>();
		ts.add("Orange");
		ts.add("Banana");
		ts.add("Apple");
		ts.add("Fig");
		ts.add("Grape");
		System.out.println(ts);
		NavigableSet<String> reverseTs = null; 
		
		reverseTs = ts.descendingSet();
		
		System.out.println(reverseTs);
		
	/*Please sort the words in the given paragraph and print it. 
	 * "The return values of navigation methods may be ambiguous 
	 * in implementations that permit null elements."
	 */
		TreeSet<String> sortedWords = new TreeSet<>();
		String myParag = "The return values of navigation methods may be ambiguous in implementations that permit null elements"; 
		String[] words = myParag.split(" ");
		
		for (int i = 0; i < words.length; i++) {
			sortedWords.add(words[i]);
		}
		
		System.out.println(sortedWords);
		
		
		
		
		
	}

}
