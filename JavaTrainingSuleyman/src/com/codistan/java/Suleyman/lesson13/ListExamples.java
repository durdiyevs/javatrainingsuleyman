package com.codistan.java.Suleyman.lesson13;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;

public class ListExamples {
	
	public static void main(String[] args) {
	
	ArrayList<Integer> myArrayList = new ArrayList<>(); 
	
	long a = new Date().getTime();
	for (int i = 0; i < 10_000; i++) {
		myArrayList.add(0, i);
	}
	long b = new Date().getTime();
	
	LinkedList<Integer> myLinkedList = new LinkedList<>();
	
	long c = new Date().getTime();
	for (int i = 0; i < 10_000; i++) {
		myLinkedList.add(0, i);
	}
	long d = new Date().getTime();
	
	System.out.println("Arraylist took " + (b-a) + " miliseconds.");
	
	System.out.println("LinkedList took " + (d-c) + " miliseconds. ");
	
	long e = new Date().getTime();
	for (int i = 0; i < 1000; i++) {
		myArrayList.remove(i);
	}
	long f = new Date().getTime();
	
	long g = new Date().getTime();
	for (int i = 0; i < 1000; i++) {
		myLinkedList.remove(i);
	}
	long h = new Date().getTime();
	
	System.out.println("Arraylist took " + (f-e) + " miliseconds.");
	
	System.out.println("LinkedList took " + (h-g) + " miliseconds. ");
	}
	
	
	


}
