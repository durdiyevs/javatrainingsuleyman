package com.codistan.java.Suleyman.lesson13;

import java.util.ArrayList;

public class WrapperSamples {

	public static void main(String[] args) {
		
		ArrayList myList = new ArrayList(); 
		
		int a = 85;
		int b = 100; 
		System.out.println(a+b);
		
		Integer intObject1 = new Integer(a);
		Integer intObject2 = new Integer(b);
		
		System.out.println(intObject1+intObject2);
		
		myList.add(a);
		myList.add(b);
		System.out.println(myList.get(1));

	}

}
