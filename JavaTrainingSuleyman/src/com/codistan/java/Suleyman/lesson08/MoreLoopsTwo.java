package com.codistan.java.Suleyman.lesson08;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MoreLoopsTwo {

	public static void main(String[] args) {
	String strParag = "Selenium is an umbrella project for a range of tools and libraries that enable and support the automation of web browsers.";
		
		String[] strArray = strParag.split(" ");
		
		for (int i = 0; i < strArray.length; i++) {
			System.out.println(strArray[i]);
		}
		
		System.out.println(strArray.length);
		
		for (String currentCycleString : strArray) {
			System.out.println(currentCycleString);
		}
		
		/*
		 * It provides extensions to emulate user interaction with browsers, a distribution server for scaling browser allocation, and the infrastructure for implementations of the W3C WebDriver specification that lets you write interchangeable code for all major web browsers.

By using the paragraph above, please print each word one by one by using enhanced loop.
		 */
		
		String secondParag = "It provides extensions to emulate user interaction with browsers, a distribution server for scaling browser allocation, and the infrastructure for implementations of the W3C WebDriver specification that lets you write interchangeable code for all major web browsers."; 
		secondParag = secondParag.replace(",", "").replace(".", "");
		String[] secondStrArray = secondParag.split(" ");
		
		for (String string : secondStrArray) {
			System.out.println(string);
		}

		
		/*
		 * reverse the paragraph above by word and print the reversed paragraph.  
		 * reverse the paragraph above by character and print the reversed paragraph. 
		 */
		
		String reverseStrByWord = ""; 
		for (int i = secondStrArray.length-1; i >=0 ; i--) {
			reverseStrByWord += secondStrArray[i] + " ";
		}
		
		reverseStrByWord = reverseStrByWord.trim(); 
		
		System.out.println(reverseStrByWord);
		
		String reverseStrByChar = "";
		
		for (int i = secondParag.length()-1; i >=0 ; i--) {
			reverseStrByChar += secondParag.charAt(i);
		}
		
		System.out.println(reverseStrByChar);
		
		StringBuilder sb = new StringBuilder(secondParag);
		sb.reverse(); 
		System.out.println(sb);
		
	}
	
	
    // Prints unique words in a string 
    static void printUniquedWords(String str) 
    { 
        // Extracting words from string 
        Pattern p = Pattern.compile("[a-zA-Z]+"); 
        Matcher m = p.matcher(str); 
          
        // Map to store count of a word 
        HashMap<String, Integer> hm = new HashMap<>(); 
          
        // if a word found 
        while (m.find())  
        { 
            String word = m.group(); 
              
            // If this is first occurrence of word 
            if(!hm.containsKey(word)) 
                hm.put(word, 1); 
            else
                // increment counter of word 
                hm.put(word, hm.get(word) + 1); 
              
        } 
          
        // Traverse map and print all words whose count 
        // is  1 
        Set<String> s = hm.keySet(); 
        Iterator<String> itr = s.iterator(); 
  
        while(itr.hasNext()) 
        { 
            String w = itr.next(); 
              
            if (hm.get(w) == 1) 
                System.out.println(w); 
        }     
    } 
}