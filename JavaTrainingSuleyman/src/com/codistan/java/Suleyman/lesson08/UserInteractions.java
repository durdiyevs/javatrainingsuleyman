package com.codistan.java.Suleyman.lesson08;

import java.util.Scanner;

public class UserInteractions {

	public static void main(String[] args) {
    
		Scanner scan = new Scanner(System.in);
		
		System.out.println("Please type your name:");
		
		String firstName = scan.nextLine();
		
        System.out.println("The first name of the user is " + firstName);
        
        
        System.out.println("Type the first number");
	
		int number1 = scan.nextInt();
		
		System.out.println("Type the second number");
		
		int number2 = scan.nextInt();
		
		int total = number1 + number2;
		
		System.out.println("Total is " + total);
	}

}
