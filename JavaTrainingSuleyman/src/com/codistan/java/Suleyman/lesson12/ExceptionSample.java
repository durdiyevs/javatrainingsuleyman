package com.codistan.java.Suleyman.lesson12;

public class ExceptionSample {

	public static void main(String[] args) {
	
		try {
			Thread.sleep(1000);
			String a="157";
			String b="3b";
			
			System.out.println(a+b);
			System.out.println(Integer.valueOf(a)+Integer.valueOf(b));
			
			System.out.println();
			
		}catch (NumberFormatException e) {
			
			e.printStackTrace();
			
		} catch (InterruptedException e) {
			
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		} finally {
			 System.out.println("Closing the resources");
		}

	}

}
