package com.codistan.java.Suleyman.lesson12;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class ExceptionSample2 {

	public static void main(String[] args) {
	
		File myFile = new File("C:\\Users\\Suleyman D\\Desktop\\FileDirectory\\Sample.txt");
		
		try {
			Scanner scanner = new Scanner(myFile);
			
			String myFirstLine = scanner.nextLine();
			String mySecondLine = scanner.nextLine();
			
			
			System.out.println(myFirstLine);
			System.out.println(mySecondLine);
			
			System.out.println(scanner.nextLine());
			System.out.println(scanner.nextLine());
//			System.out.println(scanner.nextLine());  //This is trying to read aline doesnt exist
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("This is the end of the application.");
		
	}

}
