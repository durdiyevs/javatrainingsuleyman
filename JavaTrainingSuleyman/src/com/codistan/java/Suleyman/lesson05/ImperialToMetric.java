package com.codistan.java.Suleyman.lesson05;

public class ImperialToMetric {

	public static void main(String[] args) {
		//We will give 5pound value and program will print the Kg value.
		
		/*double poundValue = 1;
		double kgValue =poundValue * 0.45;
		double conversion = poundValue * kgValue;
		double fiveTimes = 5 * conversion;*/
		
		double poundValue = 5;
		final double poundToKgConstant = 0.4535; //final makes a variable constant which means once it declared, it can not be changed 
		double resultKg = poundValue * poundToKgConstant;
		System.out.println(poundValue +" pounds = " + resultKg + " kgs ");
		
	
		
		//We will give 5inch value and program will print the cm value.
		
		double inchValue1 = 5;
		final double inchToCmConstant = 2.54; //final makes a variable constant which means once it declared, it can not be changed 
		double resultCm= inchValue1 * inchToCmConstant;
		System.out.println(inchValue1 +" inch = " + resultCm + " cm ");
		
		
		//we will give 5mile value and program will calculate the km value.
		double mileValue = 5;
		final double mileToKmConstant = 1.609; //final makes a variable constant which means once it declared, it can not be changed 
		double resultKm1 = mileValue * mileToKmConstant;
		System.out.println(mileValue +" mile = " + resultKm1 + " km ");
		
		
		
		
		
		//We will give 5oz value and program will calculate the ml value
		double ozValue1 = 5;
		final double ozToMlConstant = 29.57; //final makes a variable constant which means once it declared, it can not be changed 
		double resultML1 = ozValue1 * ozToMlConstant;
		System.out.println(ozValue1 +" oz = " + resultML1 + " ml ");
		
		//We will give 5 F value and program will calculate the C value
		
		double fValue1 = 5;
		//final double fToCConstant1 = ((fValue1 -32) * (5/9)); //final makes a variable constant which means once it declared, it can not be changed 
		double resultC1 = ((fValue1 -32) * (5/9));
		System.out.println(fValue1 +" Fahr = " + resultC1 + " Celc ");
		

	}

}
