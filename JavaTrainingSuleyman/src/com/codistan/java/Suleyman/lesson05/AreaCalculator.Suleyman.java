package com.codistan.java.Suleyman.lesson05;

public class AreaCalculator {

	public static void main(String[] args) {
		
		 /*Create a class called AreaCalculator which will calculate the area of a square, rectangle and triangle by the given dimensions and print them to the console.*/
		
		//Area of a square calculator:
		
		double heightOfSquare = 3;
		double widthOfSquare =heightOfSquare * 1 ;
		double areaOfSquare = heightOfSquare * widthOfSquare;
		String unitOfSquare = "m";
		
		System.out.println("Area of a Square is " + areaOfSquare +" "+ unitOfSquare +"^2");
		
	    //Rectangle Calculator
		
		double heightOfRectangle = 6;
		double widthOfRectangle = 3;
		double areaOfRectangle = heightOfRectangle * widthOfRectangle;
		String unitOfRectangle = "m";
		
		System.out.println("Area of a Rectangle is " + areaOfRectangle +" "+ unitOfRectangle +"^2");
		
		//Triangle Calculator
		
		double heightOfTriangle = 6;
		double baseOfTriangle = 3;
		double areaOfTriangle = (heightOfTriangle * baseOfTriangle)/2;
		String unitOfTriangle = "m";
		
		System.out.println("Area of a Triangle is " + areaOfTriangle +" "+ unitOfTriangle +"^2");
		
		 

	}

}
