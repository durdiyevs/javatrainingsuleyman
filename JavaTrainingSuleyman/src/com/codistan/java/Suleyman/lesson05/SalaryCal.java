package com.codistan.java.Suleyman.lesson05;

public class SalaryCal {

	public static void main(String[] args) {
		/*Create a class called SalaryCalculator which will calculate weekly total salary. An employee can get $13.25 per hour but if he/she works more than 40 hours than the wage will be calculated by x1.5 so more than regular wage. Application will calculate total amount based on the hours an employee worked and print it to the console.*/
		
		double salaryRate = 13.25;
		double hoursWorked = 45;
		double earnedWeeklySalary = ( salaryRate * 40) + ((salaryRate * 1.5)*(hoursWorked - 40));
		
		System.out.println("You earned $" + earnedWeeklySalary + "this week.");

	}

}
