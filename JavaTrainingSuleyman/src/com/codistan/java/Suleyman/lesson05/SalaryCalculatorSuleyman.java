package com.codistan.java.Suleyman.lesson05;

public class SalaryCalculatorSuleyman{

	public static void main(String[] args) {
		/*Create a class called SalaryCalculator which will calculate weekly total salary. An employee can get $13.25 per hour but if he/she works more than 40 hours than the wage will be calculated by x1.5 so more than regular wage. Application will calculate total amount based on the hours an employee worked and print it to the console.*/
		
		int hoursWorked = 30;
		
		int b = 40; 
        int d = 0;
        
		double wagePerHour = 13.5;                       
		double wageOverTime = wagePerHour * 1.5; 

		double regularHoursTotal = Math.min(hoursWorked, b) * wagePerHour;
        double overTimeHoursTotal = Math.max(hoursWorked, b) %40* wageOverTime;
        
        
        
        System.out.println("He/She worked " + hoursWorked + " hours."); 	
        
        System.out.println("Regular Hours " + Math.min(hoursWorked, b));
        
        System.out.println("Regular Hours " + Math.min(hoursWorked, b) * wagePerHour + "$");
        
        System.out.println("Overtime Hours " + Math.max(hoursWorked, b) %40);
        
        System.out.println("Overtime Hours " + Math.max(hoursWorked, b) %40* wageOverTime + "$");
        
        System.out.println("Total " +( regularHoursTotal + overTimeHoursTotal) +"$");
		
		
	}

}
          