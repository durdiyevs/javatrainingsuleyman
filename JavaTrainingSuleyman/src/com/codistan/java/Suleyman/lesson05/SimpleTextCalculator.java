package com.codistan.java.Suleyman.lesson05;

public class SimpleTextCalculator {

	public static void main(String[] args) {
		// We need to have two variables, one for item name, one for item price and it will print total price including Tax such as "Bread" is $2.45 . Tax rate is 10.25%
  
		String itemName;
		
		itemName = "Bread ";
		
		double bred = 2.45;
				
		double Tax =(bred *10.25/100);
		
		double totalPrice =(bred + Tax);
		
		System.out.println(itemName + "is " + totalPrice + "$");
	}

}
