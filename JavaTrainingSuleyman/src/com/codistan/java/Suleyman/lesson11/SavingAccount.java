package com.codistan.java.Suleyman.lesson11;

public class SavingAccount extends Account {
	
	public SavingAccount(String accountNo, String routingNo, String ownersFullName, double balance) {
		
		super(accountNo, routingNo, ownersFullName, balance);
	
	}

	protected void depositCash(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);
	}

	public void depositCheck(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);

	}

//	public void transfer() {
//		
//
//	}

	// balance should not go less than -$1000
	public void withdrawMoney(double amount) {

		if (getBalance() - amount >= 10000) {
			super.setBalance(getBalance() - amount);
			if(getBalance()>1000) {
				super.setBalance(getBalance() - 45);
			}
		} 
		else {
			System.out.println("Your balance can not be less than $10,000, please revise your amount.");
		}

	}

	public void setBalance(double balance) {    //overriding the  parent class method 
		System.out.println("Setting Balance is not allowed.");
	}
}
		
	