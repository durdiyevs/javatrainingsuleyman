package com.codistan.java.Suleyman.lesson11;

public class CheckingAccount extends Account {

	public CheckingAccount(String accountNo, String routingNo, String ownersFullName, double balance) {
		super(accountNo, routingNo, ownersFullName, balance);
		// TODO Auto-generated constructor stub
	}

	protected void depositCash(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);
	}

	public void depositCheck(double amount) {
//		balance = balance + amount; 
		super.setBalance(getBalance() + amount);

	}

//	public void transfer() {
//		
//
//	}

	// balance should not go less than -$1000
	public void withdrawMoney(double amount) {

		if (getBalance() - amount >= -1000) {
			super.setBalance(getBalance() - amount);
			if(getBalance()<0) {
				super.setBalance(getBalance() - 45);
			}
		} 
		else {
			System.out.println("Your balance can not be less than -$1000, please revise your amount.");
		}

	}

	public void setBalance(double balance) {
		System.out.println("Setting Balance is not allowed.");
	}

}
