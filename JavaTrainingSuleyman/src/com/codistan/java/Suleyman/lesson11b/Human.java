package com.codistan.java.Suleyman.lesson11b;

public class Human {
	
	
	final static String planet = "earth";

	private String mood;

	public Human() {
		super();
	}

	public Human(String mood) {
		super();
		this.mood = mood;
	}

	public String getMood() {
		return mood = null; // this is field .
	}

	public void setMood(String mood) { // these are method
		this.mood = mood;
	}

	public void eat() { // these are method

		String temp = null; // local variable is not a field
		String temp2 = new String();
		String temp3 = new String("Hello");
		System.out.println("Human is eating");

	}

	public void eat(String food) { // overloaded eat method
		System.out.println("Human is eating " + food);
	}

}
