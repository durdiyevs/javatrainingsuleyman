package com.codistan.java.Suleyman.lesson11b;

public class Youth extends Human {
	
	public Youth(String mood) {
		super(mood);
	}
	
	public void bike() {
		System.out.println("Youth is Biking");
	}

	@Override
	public void eat() {    //overriding parent eat method
    System.out.println("Youth is eating very slowly");
	}
	
	
	
}
