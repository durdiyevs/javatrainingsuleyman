package com.codistan.java.Suleyman.lesson11b;



public class Runner {

	public static void main(String[] args) {
		Student obj1 = new Student("happy"); 
		
		Human obj2 = new Student("very happy"); //polymorphism		
		
		obj1.study();
		obj1.bike();
		obj1.eat();
		
		obj2.eat();
		
		obj2 = new Youth("extremely happy"); 
		
		obj2.eat();
		
		Student obj3 = new Student("flying");
		
		obj3.kickBall();
		obj3.jumpHigh();  
		
		obj3.toString();
		obj1.eat();
		obj1.eat("banana");
		
		System.out.println(Human.planet );
	
	}

}
