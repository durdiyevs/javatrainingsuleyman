package com.codistan.java.Suleyman.lesson06;

public class JavaString {

	public static void main(String[] args) {
		 
		String exampleString = "Hello World";     //literal way goes to POOL  .
		
		System.out.println(exampleString);
	
	
		String exampleString2 = new String ( "Hello World");          //it  goes to memory
		
		System.out.println(exampleString2);
		
		exampleString = "Hello Class";
		
		System.out.println(exampleString);
		
		
		String exampleThree = "My name";
		
		exampleThree = exampleThree + " is John ";
		
		System.out.println(exampleThree);
		
		System.out.println(exampleThree.charAt(1));     // We are saying : Hey String , give me the second character
		
		System.out.println(exampleThree.concat("!!!"));    
		
		System.out.println(exampleThree);   //This will print My name is John only, because String methods dont change the String itself.
		
		exampleThree = exampleThree.concat("!!!");
		
		System.out.println(exampleString);
		
		System.out.println(exampleThree.contains("John"));
		
		
		
		System.out.println(exampleString);
	    
		System.out.println(exampleString.charAt(0));
	

	    
	    System.out.println(exampleString.substring(0,3));
	    
	    
	    System.out.println(exampleString.substring(1,2));
	}
	
	

}
