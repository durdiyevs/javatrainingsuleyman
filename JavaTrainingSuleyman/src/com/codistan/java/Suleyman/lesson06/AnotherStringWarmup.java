package com.codistan.java.Suleyman.lesson06;

public class AnotherStringWarmup {
	
	public static void main(String[] args) {
		
		String fullName = "John Smith";
		fullName = fullName.toUpperCase();
		String[] words = fullName.split(" ");
		fullName = words[0].substring(0,1).toLowerCase()+words[0].substring(1)+ " " + words[1].substring(0,1).toLowerCase() + words[1].substring(1);
		System.out.println(fullName);
		
		
		//without String array
		
		//String fullNameTwo = "John Smith";
		
		
		
		
		
	}

}
