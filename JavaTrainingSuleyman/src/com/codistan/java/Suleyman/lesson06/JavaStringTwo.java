package com.codistan.java.Suleyman.lesson06;

public class JavaStringTwo {

	public static void main(String[] args) {
	
		String firstStr = "John";
		String secondStr = "John";
		String thirdStr = new String ("John");
		String forthStr = new String ("John");
		String fifthStr = new String ("john");
		
		System.out.println(firstStr == secondStr);
		System.out.println(secondStr == thirdStr);
		System.out.println(thirdStr == forthStr);
		System.out.println(forthStr == fifthStr);
		System.out.println();
		System.out.println(firstStr.equals(secondStr)); //when you are comparing String values, always use equals(). 
		System.out.println(secondStr.equals(thirdStr));
		System.out.println(thirdStr.equals(forthStr));
		System.out.println(forthStr.equals(fifthStr));
		System.out.println();
		System.out.println(); 
		
		
		
		
		
		String seventhStr = "Hello World";
		System.out.println(seventhStr.replaceAll( "l", "t"));
		String[] strArray = seventhStr.split("");
		System.out.println(strArray [0]);
		System.out.println(strArray [1]);
		System.out.println(seventhStr.endsWith("t"));
		
		
		
		
		String eightStr = "John ";
		System.out.println(eightStr);
		System.out.println(eightStr.trim());
		

	}

}
