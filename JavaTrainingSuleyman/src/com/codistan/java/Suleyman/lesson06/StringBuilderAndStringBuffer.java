package com.codistan.java.Suleyman.lesson06;

public class StringBuilderAndStringBuffer {

	public static void main(String[] args) {
		
		StringBuilder sb01 = new StringBuilder("Hello World");
		
	    System.out.println(sb01);
	    
	    System.out.println(sb01.append(" and Class"));   //append changes the Implicit 
	     
	    System.out.println(sb01);
	}

}
